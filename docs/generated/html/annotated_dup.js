var annotated_dup =
[
    [ "COFFHeader", "structCOFFHeader.html", "structCOFFHeader" ],
    [ "DataDirectory", "structDataDirectory.html", "structDataDirectory" ],
    [ "OptionalHeader", "structOptionalHeader.html", "structOptionalHeader" ],
    [ "PEFileHeader", "structPEFileHeader.html", "structPEFileHeader" ],
    [ "SectionHeader", "structSectionHeader.html", "structSectionHeader" ],
    [ "SectionRawData", "structSectionRawData.html", "structSectionRawData" ],
    [ "StandartCOFFFields", "structStandartCOFFFields.html", "structStandartCOFFFields" ],
    [ "WindowsSpecificFields", "structWindowsSpecificFields.html", "structWindowsSpecificFields" ]
];