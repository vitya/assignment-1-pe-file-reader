var File_8h =
[
    [ "COFFHeader", "structCOFFHeader.html", "structCOFFHeader" ],
    [ "StandartCOFFFields", "structStandartCOFFFields.html", "structStandartCOFFFields" ],
    [ "WindowsSpecificFields", "structWindowsSpecificFields.html", "structWindowsSpecificFields" ],
    [ "DataDirectory", "structDataDirectory.html", "structDataDirectory" ],
    [ "OptionalHeader", "structOptionalHeader.html", "structOptionalHeader" ],
    [ "SectionHeader", "structSectionHeader.html", "structSectionHeader" ],
    [ "PEFileHeader", "structPEFileHeader.html", "structPEFileHeader" ],
    [ "SectionRawData", "structSectionRawData.html", "structSectionRawData" ],
    [ "DataDirectoryType", "File_8h.html#a97389a735328a5f8cf294478f704b602", [
      [ "DATA_DIRECTORY_EXPORT_TABLE", "File_8h.html#a97389a735328a5f8cf294478f704b602ad0d93c3deb4bc8032cde482f1102cbb1", null ],
      [ "DATA_DIRECTORY_IMPORT_TABLE", "File_8h.html#a97389a735328a5f8cf294478f704b602a4478000aaf096dd163ad2c7612c2a5ff", null ],
      [ "DATA_DIRECTORY_RESOURCE_TABLE", "File_8h.html#a97389a735328a5f8cf294478f704b602ad61ff4147de2ce80af3ebe87505c9045", null ],
      [ "DATA_DIRECTORY_EXCEPTION_TABLE", "File_8h.html#a97389a735328a5f8cf294478f704b602aed40660a066f931c340500d4c0c7f9db", null ],
      [ "DATA_DIRECTORY_CERTIFICATE_TABLE", "File_8h.html#a97389a735328a5f8cf294478f704b602a290f4ee40d1d7644fbc8951d949f3a1b", null ],
      [ "DATA_DIRECTORY_BASE_RELOCATION_TABLE", "File_8h.html#a97389a735328a5f8cf294478f704b602a7f0599332e46b1241ce7ef7f3521bed5", null ],
      [ "DATA_DIRECTORY_DEBUG", "File_8h.html#a97389a735328a5f8cf294478f704b602aaeef3abf72793a48c8e72cd8d35951cb", null ],
      [ "DATA_DIRECTORY_ARCHITECTURE_DATA", "File_8h.html#a97389a735328a5f8cf294478f704b602ae6c7cbebfb182e92cbfdace75ed5b9ac", null ],
      [ "DATA_DIRECTORY_GLOBAL_PTR", "File_8h.html#a97389a735328a5f8cf294478f704b602ae531571e2eb0c1be18a6decaabdd7c14", null ],
      [ "DATA_DIRECTORY_TLS_TABLE", "File_8h.html#a97389a735328a5f8cf294478f704b602a251969340b0e5887d28f304d64231432", null ],
      [ "DATA_DIRECTORY_LOAD_CONFIG_TABLE", "File_8h.html#a97389a735328a5f8cf294478f704b602aa82eb91a457f48f661cc0fedc648003d", null ],
      [ "DATA_DIRECTORY_BOUND_IMPORT", "File_8h.html#a97389a735328a5f8cf294478f704b602a8ab086418cf756753e3fe2bd090a5b72", null ],
      [ "DATA_DIRECTORY_IMPORT_ADDRESS_TABLE", "File_8h.html#a97389a735328a5f8cf294478f704b602ae1db65c0c87a58718bc1028d05b1b515", null ],
      [ "DATA_DIRECTORY_DELAY_IMPORT_DESCRIPTOR", "File_8h.html#a97389a735328a5f8cf294478f704b602abc0f4d213b6971347313d517445ec4f9", null ],
      [ "DATA_DIRECTORY_CLR_RUNTIME_HEADER", "File_8h.html#a97389a735328a5f8cf294478f704b602afed00e4bd4e58b27e59c0bf8b791a5b4", null ]
    ] ]
];