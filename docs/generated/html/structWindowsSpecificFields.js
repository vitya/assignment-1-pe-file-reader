var structWindowsSpecificFields =
[
    [ "CheckSum", "structWindowsSpecificFields.html#ae805858cb463ae5e05f7ace8c0626856", null ],
    [ "DllCharacteristics", "structWindowsSpecificFields.html#adb8bc1819d39b473d8c49edf3e3508bc", null ],
    [ "FileAlignment", "structWindowsSpecificFields.html#a866597ff48269fa995b2efbcc1b5e07e", null ],
    [ "ImageBase", "structWindowsSpecificFields.html#a407bf10efe30b6ce95587c868d1011c3", null ],
    [ "LoaderFlags", "structWindowsSpecificFields.html#abc16ad62081c1455f1500d6cdd076632", null ],
    [ "MajorImageVersion", "structWindowsSpecificFields.html#aeed5ec3d908aa2854d452b53f2a5f76f", null ],
    [ "MajorOperatingSystemVersion", "structWindowsSpecificFields.html#a62137b6e34ca71c39af83107802d3c89", null ],
    [ "MajorSubsystemVersion", "structWindowsSpecificFields.html#aa1c47401c277d0b30f834b7811107bd8", null ],
    [ "MinorImageVersion", "structWindowsSpecificFields.html#ab99c85c367892a3d1023ef020df57384", null ],
    [ "MinorOperatingSystemVersion", "structWindowsSpecificFields.html#a57e6b7ea2c078f96e4a3d5e334f70581", null ],
    [ "MinorSubsystemVersion", "structWindowsSpecificFields.html#a8cbcd816ad203d0eb0da593af8d6bdda", null ],
    [ "NumberOfRvaAndSizes", "structWindowsSpecificFields.html#a296397b91b3d7358fb5e74f62b9dca03", null ],
    [ "SectionAligment", "structWindowsSpecificFields.html#a48c019466eea2ee227682057b6b9aeff", null ],
    [ "SizeOfHeaders", "structWindowsSpecificFields.html#ae81a4eca1a1d653e89e3044243c3c013", null ],
    [ "SizeOfHeapCommit", "structWindowsSpecificFields.html#a249bf987561af9f836485e6ca917531f", null ],
    [ "SizeOfHeapReserve", "structWindowsSpecificFields.html#aeff39dd412175b33e509506fc283bb54", null ],
    [ "SizeOfImage", "structWindowsSpecificFields.html#a0de1994714251743d256bdac7fb17bb8", null ],
    [ "SizeOfStackCommit", "structWindowsSpecificFields.html#a3af4ff43e5a2766b42d8016f1a582198", null ],
    [ "SizeOfStackReserve", "structWindowsSpecificFields.html#a687c7b5025d9de1772d75da10d03bd3e", null ],
    [ "Subsystem", "structWindowsSpecificFields.html#a9d265ebb0343ca2fa15c8e1a9805921a", null ],
    [ "Win32VersionValue", "structWindowsSpecificFields.html#aaacdbfd6945fd01e5f2ded94a6a6332c", null ]
];