var structSectionHeader =
[
    [ "Characteristics", "structSectionHeader.html#aac07a108196fb488a5b58ae106d8a4a0", null ],
    [ "Name", "structSectionHeader.html#ab74f712c34be360d78fc2a1adf4b1842", null ],
    [ "NumberOfLinenumbers", "structSectionHeader.html#a798c5dee1fb9fa69edf1aee1df5cda67", null ],
    [ "NumberOfRelocations", "structSectionHeader.html#a83c4380a5da9362df2073de49c685c0c", null ],
    [ "PointerToLinenumbers", "structSectionHeader.html#a3e9d87a333cbbc77b540b7849b5b4404", null ],
    [ "PointerToRawData", "structSectionHeader.html#addb84e846f9da87321aae7722f491f99", null ],
    [ "PointerToRelocations", "structSectionHeader.html#a607b33d31e87959a211022c386bd4d05", null ],
    [ "SizeOfRawData", "structSectionHeader.html#aeef4fef403e47b67aaead1faa6933f4c", null ],
    [ "VirtualAddress", "structSectionHeader.html#a5a91bebf43c695ffc8950bdf9c6cfe9b", null ],
    [ "VirtualSize", "structSectionHeader.html#ac3ff5a8bd7151622f3b7b24b869b0fe1", null ]
];