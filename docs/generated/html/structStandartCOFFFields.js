var structStandartCOFFFields =
[
    [ "AddressOfEntryPoint", "structStandartCOFFFields.html#a4d28a5cbc01657997f858b157033b8cd", null ],
    [ "BaseOfCode", "structStandartCOFFFields.html#a5d067a71f737c01cffe07ee746cab2c9", null ],
    [ "BaseOfData", "structStandartCOFFFields.html#a4c27b5e395ce0dd761c38405d6705dac", null ],
    [ "Magic", "structStandartCOFFFields.html#ae208bc9d7890a384461763a84f5edfd7", null ],
    [ "MajorLinkerVersion", "structStandartCOFFFields.html#a74e7ba1e68bfe5f5d5d4810f78c0aace", null ],
    [ "MinorLinkerVersion", "structStandartCOFFFields.html#a5cb254e192a6f178767c4f5d3af5b2b2", null ],
    [ "SizeOfCode", "structStandartCOFFFields.html#abce1ae89e782187790644fd5eb7ef141", null ],
    [ "SizeOfInitializedData", "structStandartCOFFFields.html#af24301e571192ff4cd82e3280522b5ba", null ],
    [ "SizeOfUninitializedData", "structStandartCOFFFields.html#a16004b4c34aed5d4bb6d66d6e88f38ee", null ]
];