var Core_8h =
[
    [ "DeclareResultOfType", "Core_8h.html#ace2a5b0d640fafed24abb4eb8aa65ceb", null ],
    [ "DefineResultOfType", "Core_8h.html#a28edc37441aaeeae3cf704312eb9d346", null ],
    [ "Result", "Core_8h.html#aa6c07af4e57b38370f3b0f2a9657a0a8", null ],
    [ "ResultFailure", "Core_8h.html#a3517afccd54a7ba0e2ca1c16059e1565", null ],
    [ "ResultSuccess", "Core_8h.html#a1f915575f7baedb9b26b395c0a9613db", null ],
    [ "ASCIIChar", "Core_8h.html#ae2e834b35674b6c572fa058e426a8276", null ],
    [ "UInt16", "Core_8h.html#a8c7e64bfcbd67be1699ac1e4d2a8d6cd", null ],
    [ "UInt32", "Core_8h.html#a7e8aeec7b6541935dfc6f608cd5170ce", null ],
    [ "UInt64", "Core_8h.html#aa288382a207683aa38aca516f6edd66d", null ],
    [ "UInt8", "Core_8h.html#af09fb0962ef29a291e5f31a6e9b679e4", null ],
    [ "ResultStatus", "Core_8h.html#a573d479910e373f5d771d303e440587d", [
      [ "SUCCESS", "Core_8h.html#a573d479910e373f5d771d303e440587dac7f69f7c9e5aea9b8f54cf02870e2bf8", null ],
      [ "FAILURE", "Core_8h.html#a573d479910e373f5d771d303e440587daa5571864412c8275a2e18a931fddcaa6", null ]
    ] ]
];