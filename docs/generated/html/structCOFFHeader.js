var structCOFFHeader =
[
    [ "Characteristics", "structCOFFHeader.html#ae8834580c0866d7ca1c09c7767da01bc", null ],
    [ "Machine", "structCOFFHeader.html#a949fcfca07056f16de9331fbc03c3997", null ],
    [ "NumberOfSections", "structCOFFHeader.html#ab63e63a9a209bc66974caab49a42164d", null ],
    [ "NumberOfSymbols", "structCOFFHeader.html#ae025b71abf1eb327c6796788a57774f5", null ],
    [ "PointerToSymbolTable", "structCOFFHeader.html#a8e4771e4ac1601a69d6db063db149663", null ],
    [ "Signature", "structCOFFHeader.html#a75b80337e0b7b35ff112b59e6918ef16", null ],
    [ "SizeOfOptionalHeader", "structCOFFHeader.html#ac8a1715d1c617556de5896f63ec8c44a", null ],
    [ "TimeDateStamp", "structCOFFHeader.html#a506c89df4d919164b445b2c087c314c2", null ]
];