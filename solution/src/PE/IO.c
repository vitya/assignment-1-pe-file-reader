/// \file
/// \brief PE File IO implementation

#include "PE/Core.h"
#include "PE/File.h"
#include "PE/IO.h"
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

DefineResultOfType(PEFileHeader);
DefineResultOfType(SectionRawData);

/// \brief Offset where PE File Header starts.
#define OFFSET_SIGNATURE_OFFSET 0x3C

Result(PEFileHeader) ReadPEFileHeader(FILE* source) {
  PEFileHeader file;

  if (fseek(source, OFFSET_SIGNATURE_OFFSET, SEEK_SET) != 0) {
    return ResultFailure(PEFileHeader)("fseek to 0x3C failed");
  }

  if (fread(
          &file.COFFHeaderOffset,
          sizeof(file.COFFHeaderOffset),
          1,
          source
      )
      < 1) {
    return ResultFailure(PEFileHeader)("fread COFFHeaderOffset failed"
    );
  }

  if (fseek(source, file.COFFHeaderOffset, SEEK_SET) != 0) {
    return ResultFailure(PEFileHeader
    )("fseek to COFFHeaderOffset failed");
  }

  if (fread(&file.COFFHeader, sizeof(COFFHeader), 1, source) < 1) {
    return ResultFailure(PEFileHeader)("fread COFFHeader failed");
  }

  if (fread(&file.OptionalHeader, sizeof(OptionalHeader), 1, source)
      < 1) {
    return ResultFailure(PEFileHeader)("fread OptionalHeader failed");
  }

  const uint32_t sectionsOffset
      = file.COFFHeaderOffset + sizeof(COFFHeader)
        + file.COFFHeader.SizeOfOptionalHeader;
  if (fseek(source, sectionsOffset, SEEK_SET) != 0) {
    return ResultFailure(PEFileHeader
    )("fseek to sections offset failed");
  }

  const size_t sectionsCount = file.COFFHeader.NumberOfSections;

  file.SectionHeaders
      = (SectionHeader*)malloc(sectionsCount * sizeof(SectionHeader));
  if (file.SectionHeaders == NULL) {
    return ResultFailure(PEFileHeader)("malloc SectionHeaders failed"
    );
  }

  if (fread(
          file.SectionHeaders,
          sizeof(SectionHeader),
          sectionsCount,
          source
      )
      < sectionsCount) {
    free(file.SectionHeaders);
    return ResultFailure(PEFileHeader)("fread SectionHeaders failed");
  }

  return ResultSuccess(PEFileHeader)(file);
}

Result(SectionRawData)
    ReadSectionRawData(FILE* file, SectionHeader header) {
  UInt8* data = malloc(header.SizeOfRawData);
  if (data == NULL) {
    return ResultFailure(SectionRawData)("malloc section data failed"
    );
  }
  if (fseek(file, header.PointerToRawData, SEEK_SET) != 0) {
    free(data);
    return ResultFailure(SectionRawData
    )("fseek to section raw data failed");
  }
  if (fread(data, header.SizeOfRawData, 1, file) < 1) {
    free(data);
    return ResultFailure(SectionRawData)("fread raw data failed");
  }
  return ResultSuccess(SectionRawData
  )((SectionRawData){.Size = header.SizeOfRawData, .Data = data});
}
