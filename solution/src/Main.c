/// \file
/// \brief Main application file

#include "PE/Core.h"
#include "PE/File.h"
#include "PE/IO.h"
#include "Walkaround.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// \brief Application name string
#define APP_NAME "section-extractor"

/// \brief Shorthand for closing files.
/// Makes current function return 1.
/// Use it only in main function or 
/// make sure that you disposed all
/// resources before calling it
/// as it can produce memory leaks.
/// \param[in] file is FILE* - openned stream to close 
/// \param[in] file is const char* - message in case of failure
/// \return 1
#define CLOSE_OR_DIE(file, message)                                  \
  if (fclose(file) != 0) {                                           \
    fprintf(stderr, message);                                        \
    return 1;                                                        \
  }

/// \brief Print usage test
/// \param[in] f File to print to (e.g., stdout)
void Usage(FILE* out) {
  fprintf(
      out, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n"
  );
}

/// \brief Finds section header by its name
/// \param[in] sectionName null-terminated string
/// \param[in] sectionHeaders array of section headers
/// \param[in] numberOfSections size of sectionHeaders array
/// \param[in] result where to put result in case of sucess
/// \return 0 if and only if success
int FindHeaderByName(
    const char* sectionName,
    SectionHeader* sectionHeaders,
    UInt16 numberOfSections,
    SectionHeader* result
) {
  for (size_t i = 0; i < numberOfSections; ++i) {
    SectionHeader header = sectionHeaders[i];
    char name[9] = {0};
    MemoryCopy(name, sizeof(name), header.Name, sizeof(header.Name));
    if (strcmp(name, sectionName) == 0) {
      *result = header;
      return 0;
    }
  }
  return 1;
}

/// \brief Application entry point
/// \param[in] argc Number of command line arguments
/// \param[in] argv Command line arguments
/// \return 0 in case of success or error code
int main(int argc, char** argv) {
  if (argc != 4) {
    Usage(stdout);
    return 0;
  }

  const char* inputFilepath = argv[1];
  const char* sectionName = argv[2];
  const char* outputFilepath = argv[3];

  FILE* inputFile = fopen(inputFilepath, "r");
  if (inputFile == NULL) {
    fprintf(stderr, "Error: can't open a file ");
    fprintf(stderr, "%s", inputFilepath);
    return 1;
  }

  Result(PEFileHeader) peFileHeaderResult
      = ReadPEFileHeader(inputFile);
  if (peFileHeaderResult.Status == FAILURE) {
    fprintf(
        stderr, "Error: %s", peFileHeaderResult.AsFailure.Message
    );
    return 1;
  }
  PEFileHeader peFile = peFileHeaderResult.AsSuccess.Value;

  if (peFile.COFFHeader.Signature[0] != 'P'
      || peFile.COFFHeader.Signature[1] != 'E'
      || peFile.COFFHeader.Signature[2] != 0
      || peFile.COFFHeader.Signature[3] != 0) {
    free(peFile.SectionHeaders);
    fprintf(stderr, "Error: invalid signature");
    return 1;
  }

  SectionHeader header;
  if (FindHeaderByName(
          sectionName,
          peFile.SectionHeaders,
          peFile.COFFHeader.NumberOfSections,
          &header
      )
      != 0) {
    free(peFile.SectionHeaders);
    fprintf(stderr, "Error: header not found");
    return 1;
  }

  Result(SectionRawData) sectionRawDataResult
      = ReadSectionRawData(inputFile, header);
  CLOSE_OR_DIE(inputFile, "Error: can't close input file");
  if (sectionRawDataResult.Status == FAILURE) {
    free(peFile.SectionHeaders);
    fprintf(
        stderr, "Error: %s\n", sectionRawDataResult.AsFailure.Message
    );
    return 1;
  }
  SectionRawData section = sectionRawDataResult.AsSuccess.Value;

  FILE* outputFile = fopen(outputFilepath, "wb");
  if (outputFilepath == NULL) {
    free(peFile.SectionHeaders);
    free(section.Data);
    fprintf(stderr, "Error: can't open output file");
    return 1;
  }

  if (fwrite(section.Data, section.Size, 1, outputFile) < 1) {
    CLOSE_OR_DIE(outputFile, "Error: can't close output file");
    free(peFile.SectionHeaders);
    free(section.Data);
    fprintf(stderr, "Error: write to file failed");
    return 1;
  }

  CLOSE_OR_DIE(outputFile, "Error: can't close output file");
  free(peFile.SectionHeaders);
  free(section.Data);
  return 0;
}
