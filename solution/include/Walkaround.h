/// \file
/// \brief Walkaround for some warnings

#ifndef _WALKAROUND_H_
#define _WALKAROUND_H_

#include <stddef.h>
#include <stdint.h>

/// \deprecated This is a walkaround of warnings with 
///             memcpy and memcpy_s. Use memcpy_s instead. 
/// \brief Copies momory from one location to another.
/// \param[in] dest Buffer address where to copy data
/// \param[in] dest_size Size of dest buffer
/// \param[in] src Buffer address from where to take data
/// \param[in] src_size Size of src buffer
static void MemoryCopy(
    void* dest, size_t dest_size, const void* src, size_t src_size
) {
  uint8_t* destination = dest;
  const uint8_t* source = src;
  for (size_t i = 0; i < src_size && i < dest_size; ++i) {
    destination[i] = source[i];
  }
}

#endif
