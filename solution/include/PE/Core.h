/// \file
/// \brief PE Core datatypes.

#ifndef _PE_CORE_H_
#define _PE_CORE_H_

#include <stdint.h>

/// \brief Alias to be consistent with windows code style.
typedef uint8_t UInt8;

/// \brief Alias to be consistent with windows code style
typedef uint16_t UInt16;

/// \brief Alias to be consistent with windows code style.
typedef uint32_t UInt32;

/// \brief Alias to be consistent with windows code style.
typedef uint64_t UInt64;

/// \brief Alias to be consistent with windows code style.
typedef uint8_t ASCIIChar;

/// \brief Result status.
typedef enum { SUCCESS, FAILURE } ResultStatus;

/// \brief Macros for declaration of template structure Result.
///
/// Use it in .h files.
///
/// enum Result<T> {
///   Success(value: R)
///   Failure(message: String)
/// }
#define DeclareResultOfType(T)                                       \
  typedef struct {                                                   \
    ResultStatus Status;                                             \
    union {                                                          \
      struct {                                                       \
        T Value;                                                     \
      } AsSuccess;                                                   \
      struct {                                                       \
        char* Message;                                               \
      } AsFailure;                                                   \
    };                                                               \
  } Result##T

/// \brief Macros for definition of template structure Result.
///
/// Use it in .c files.
#define DefineResultOfType(T)                                        \
  static Result##T Result##T##Success(T value) {                     \
    return (Result##T                                                \
    ){.Status = SUCCESS, .AsSuccess = {.Value = value}};             \
  }                                                                  \
                                                                     \
  static Result##T Result##T##Failure(char* message) {               \
    return (Result##T                                                \
    ){.Status = FAILURE, .AsFailure = {.Message = message}};         \
  }                                                                  \
                                                                     \
  int __suppress_warning__

/// \brief Alias for genric-style type.
///
/// Usage: Result(User) userOrError = ReadUser();
#define Result(T) Result##T

/// \brief Alias for genric-style Success contructor-function.
///
/// Usage: return ResultSuccess(User)(user);
#define ResultSuccess(T) Result##T##Success

/// \brief Alias for genric-style Failure contructor-function.
///
/// Usage: return ResultFailure(User)("user reading failed");
#define ResultFailure(T) Result##T##Failure

#endif
