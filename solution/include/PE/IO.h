/// \file
/// \brief PE File content reading utils

#ifndef _PE_IO_H_
#define _PE_IO_H_

#include "Core.h"
#include "File.h"
#include <stdio.h>

/// \brief Declare required Result<PEFileHeader>.
DeclareResultOfType(PEFileHeader);

/// \brief Declare required Result<SectionRawData>.
DeclareResultOfType(SectionRawData);

/// \brief Reads PE File Header from stream
/// \param[in] file stream from where to read
/// \return either Success of Failure
Result(PEFileHeader) ReadPEFileHeader(FILE* file);

/// \brief Reads Section Raw Data
/// \param[in] file stream from where to read
/// \param[in] header section header what we want to read
/// \return either Success of Failure
Result(SectionRawData)
    ReadSectionRawData(FILE* file, SectionHeader header);

#endif
