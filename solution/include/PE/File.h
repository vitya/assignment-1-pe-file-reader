/// \file
/// \brief PE File headers structures

#ifndef _PE_FILE_H_
#define _PE_FILE_H_

#include "Core.h"
#include <stdint.h>

/// \brief COFF Header
typedef struct __attribute__((packed)) {
  ASCIIChar Signature[4];
  UInt16 Machine;
  UInt16 NumberOfSections;
  UInt32 TimeDateStamp;
  UInt32 PointerToSymbolTable;
  UInt32 NumberOfSymbols;
  UInt16 SizeOfOptionalHeader;
  UInt16 Characteristics;
} COFFHeader;

/// \brief Standart COFF Fields
typedef struct __attribute__((packed)) {
  UInt16 Magic;
  UInt8 MajorLinkerVersion;
  UInt8 MinorLinkerVersion;
  UInt32 SizeOfCode;
  UInt32 SizeOfInitializedData;
  UInt32 SizeOfUninitializedData;
  UInt32 AddressOfEntryPoint;
  UInt32 BaseOfCode;
  UInt32 BaseOfData;
} StandartCOFFFields;

/// \brief Windows Specific Fields
typedef struct __attribute__((packed)) {
  UInt32 ImageBase;
  UInt32 SectionAligment;
  UInt32 FileAlignment;
  UInt16 MajorOperatingSystemVersion;
  UInt16 MinorOperatingSystemVersion;
  UInt16 MajorImageVersion;
  UInt16 MinorImageVersion;
  UInt16 MajorSubsystemVersion;
  UInt16 MinorSubsystemVersion;
  UInt32 Win32VersionValue;
  UInt32 SizeOfImage;
  UInt32 SizeOfHeaders;
  UInt32 CheckSum;
  UInt16 Subsystem;
  UInt16 DllCharacteristics;
  UInt32 SizeOfStackReserve;
  UInt32 SizeOfStackCommit;
  UInt32 SizeOfHeapReserve;
  UInt32 SizeOfHeapCommit;
  UInt32 LoaderFlags;
  UInt32 NumberOfRvaAndSizes;
} WindowsSpecificFields;

/// \brief Data Directory
typedef struct __attribute__((packed)) {
  UInt32 VirtualAddress;
  UInt32 Size;
} DataDirectory;

/// \brief Optional Header
typedef struct __attribute__((packed)) {
  StandartCOFFFields StandartCOFFFields;
  WindowsSpecificFields WindowsSpecificFields;
  DataDirectory DataDirectories[16];
} OptionalHeader;

/// \brief Types of Data Directories
///        values are indexes in 
///        OptionalHeader::DataDirectories array
typedef enum {
  DATA_DIRECTORY_EXPORT_TABLE = 0,
  DATA_DIRECTORY_IMPORT_TABLE,
  DATA_DIRECTORY_RESOURCE_TABLE,
  DATA_DIRECTORY_EXCEPTION_TABLE,
  DATA_DIRECTORY_CERTIFICATE_TABLE,
  DATA_DIRECTORY_BASE_RELOCATION_TABLE,
  DATA_DIRECTORY_DEBUG,
  DATA_DIRECTORY_ARCHITECTURE_DATA,
  DATA_DIRECTORY_GLOBAL_PTR,
  DATA_DIRECTORY_TLS_TABLE,
  DATA_DIRECTORY_LOAD_CONFIG_TABLE,
  DATA_DIRECTORY_BOUND_IMPORT,
  DATA_DIRECTORY_IMPORT_ADDRESS_TABLE,
  DATA_DIRECTORY_DELAY_IMPORT_DESCRIPTOR,
  DATA_DIRECTORY_CLR_RUNTIME_HEADER
} DataDirectoryType;

/// \brief Section Header
typedef struct __attribute__((packed)) {
  ASCIIChar Name[8];
  UInt32 VirtualSize;
  UInt32 VirtualAddress;
  UInt32 SizeOfRawData;
  UInt32 PointerToRawData;
  UInt32 PointerToRelocations;
  UInt32 PointerToLinenumbers;
  UInt16 NumberOfRelocations;
  UInt16 NumberOfLinenumbers;
  UInt32 Characteristics;
} SectionHeader;

/// \brief PE File Header
typedef struct {
  UInt32 COFFHeaderOffset;
  COFFHeader COFFHeader;
  OptionalHeader OptionalHeader;
  SectionHeader* SectionHeaders;
} PEFileHeader;

/// \brief Section Raw Data.
/// You should call free(SectionRawData::Data) 
/// after usage.
typedef struct {
  UInt32 Size;
  UInt8* Data;
} SectionRawData;

#endif
